import { useRecoilValue } from "recoil";
import { HomeApi, ScenarioApi, SceneApi, Scene } from "../api/typings";
import { getHomesFromApi, scenesApiState, getCreditsFromApi } from "../state";
import { getCreditsByRole, getScenarioCredits, setCredits } from "../components/Credits";

let homeScenarios: ScenarioApi[];
let homeScenesList: Record<string, Scene>;
let sceneIntro: Scene;

export const getScene = (name: string): Scene => homeScenesList[name] ?? homeScenesList[name + "01"];

export const getSceneFromList = (name: string, list: Record<string, Scene>): Scene => list[name] ?? list[name + "01"];

export const getSceneInfoFromUrl = (url: string): { scenario: string; order: number } => {
  if (!url.includes("play")) return { scenario: "", order: 1 };
  const str = url.split("tact-ui/")[1].split("/play")[0];
  const identifier = str.slice(0, -3);
  const order = parseInt(str.split(identifier + "-")[1]);
  const sceneInfo = {
    scenario: identifier,
    order: order,
  };
  return sceneInfo;
};

export const getSceneIntro = (): Scene => {
  return sceneIntro;
};

export const getPanoramaScene = (): Scene => {
  const keys = Object.keys(homeScenesList);
  const panoramaSceneName = keys.filter((key) => key.startsWith("panorama") && key.endsWith("01"))[0];
  const panoramaScene = getScene(panoramaSceneName);
  return panoramaScene;
};

export const getScenarioDuration = (id: string): number => {
  for (let i = 0; i < homeScenarios.length; i++) {
    if (homeScenarios[i].identifier == id) {
      return homeScenarios[i].credit_duration;
    }
  }
  return 10;
};

export function getScenesFromApi(): Record<string, Scene> {
  // const intro01: Scene = {
  //   name: "intro-01",
  //   title: "TACT",
  //   scenario: undefined,
  //   scenarioIdentifier: "intro-01",
  //   acknowledgements: "",
  //   composer: "Romain Barthélémy",
  //   assistantComposer: "",
  //   designer: "Zoé Aegerter",
  //   photographer: "Quentin Chevrier",
  //   code: "Pascal Vaccaro",
  //   imageSrc: process.env.PUBLIC_URL + "/images/3840/intro-01.png",
  //   wetImageSrc: process.env.PUBLIC_URL + "/images/3840/intro-02.png",
  //   duration: 1,
  //   transitionOut: 1000,
  //   transitionColor: 1,
  //   order: 1,
  //   next: "credits",
  //   totalScenesNum: 1,
  //   wet_fx: false,
  //   graphFxOnMove: {
  //     id: 1,
  //     accuracy: 0.7,
  //     brush_radius: 40,
  //     enabled: true,
  //     hardness: 0.5,
  //   },
  //   graphFxAfterMove: {
  //     id: 1,
  //     brush_radius: 40,
  //     enabled: true,
  //   },
  //   graphFxAfterTap: {
  //     id: 1,
  //     brush_radius: 40,
  //     enabled: false,
  //     rain_enabled: true,
  //     after_tap_fx: 0,
  //   },
  //   graphFxOnHold: {
  //     id: 1,
  //     enabled: false,
  //     max_radius: 40,
  //     max_width: 40,
  //     max_height: 40,
  //     shape: 0,
  //     display_speed: 2,
  //   },
  // };

  const credits = {
    name: "credits",
    scenarioIdentifier: "credits",
    duration: 10,
  } as Scene;

  const allCredits = useRecoilValue(getCreditsFromApi);

  const homes = useRecoilValue(getHomesFromApi);
  // console.log("HOMES:", homes);

  const selectedHome: HomeApi[] = homes.filter((home) => home.enabled === true);
  // console.log("SELECTED HOME: ", selectedHome);

  if (selectedHome.length) {
    const scenarios: ScenarioApi[] = selectedHome[0].scenario;
    // console.log("SELECTED HOME SCENARIOS: ", scenarios);
    homeScenarios = scenarios;

    // const allScenarios = useRecoilValue(getScenariosFromApi);
    // console.log("ALL SCENARIOS: ", allScenarios);

    const allScenes: SceneApi[] = useRecoilValue(scenesApiState);
    // console.log("ALL SCENES: ", allScenes);

    const selectedScenes: [string, Scene][] = [];

    // get scenarios scenes count in order to select next scene from scene order
    const scenarioScenesCount: Record<string, number> = {};
    if (scenarios) {
      for (let i = 0; i < scenarios.length; i++) {
        scenarioScenesCount[scenarios[i].identifier] = 0;

        for (let j = 0; j < allScenes.length; j++) {
          if (allScenes[j].scenario) {
            if (scenarios[i].identifier === allScenes[j].scenario.identifier) {
              scenarioScenesCount[scenarios[i].identifier]++;
            }
          }
        }
      }
    } else {
      scenarioScenesCount[""] = 0;
    }
    // console.log("SCENARIOS SCENES COUNT: ", scenarioScenesCount);

    // Filter all scenarios to get intro
    const scenarioIntro = scenarios.filter((scenario) => scenario.is_intro === true);
    // console.log("SCENARIO INTRO: ", scenarioIntro);
    // if (!scenarioIntro.length && scenarios.length) {
    //   scenarioIntro.push(scenarios[0]);
    // }

    // Filter all scenes and get only first from each selected scenario
    scenarios.forEach((scenarioElement: ScenarioApi) => {
      allScenes.forEach((sceneElement: SceneApi) => {
        if (sceneElement.scenario) {
          if (scenarioElement.status === 1 && scenarioElement.identifier === sceneElement.scenario.identifier) {
            const s: Scene = {
              name: setIdentifier(scenarioElement, sceneElement, false),
              title: sceneElement.name,
              scenario: sceneElement.scenario,
              scenarioIdentifier: scenarioElement.identifier,
              composer: sceneElement.authors[0] ? sceneElement.authors[0].username : "",
              designer: sceneElement.authors[0] ? sceneElement.authors[0].username : "",
              photographer: sceneElement.authors[0] ? sceneElement.authors[0].username : "",
              assistantComposer: sceneElement.authors[0] ? sceneElement.authors[0].username : "",
              code: sceneElement.authors[0] ? sceneElement.authors[0].username : "",
              acknowledgements: "",
              imageSrc:
                sceneElement.foreground_image !== null
                  ? sceneElement.foreground_image.file
                  : process.env.PUBLIC_URL + "/images/3840/default-scene.png",
              wetImageSrc:
                sceneElement.background_image !== null
                  ? sceneElement.background_image.file
                  : process.env.PUBLIC_URL + "/images/3840/default-scene.png",
              order: sceneElement.order,
              totalScenesNum: scenarioScenesCount[scenarioElement.identifier],
              duration: toSeconds(sceneElement.next_duration), // timer duration, in seconds
              // duration: sceneElement.next_duration,
              transitionOut: sceneElement.transition_duration * 1000, // transition from one scene to the next, in ms
              // transitionOut: sceneElement.transition_duration,
              transitionColor: sceneElement.transition_color,
              next:
                sceneElement.order < scenarioScenesCount[scenarioElement.identifier]
                  ? setIdentifier(scenarioElement, sceneElement, true)
                  : "credits",
              wet_fx: sceneElement.wet_fx,
              graphFxOnMove: sceneElement.graph_fx_on_move,
              graphFxAfterMove: sceneElement.graph_fx_after_move,
              graphFxAfterTap: sceneElement.graph_fx_after_tap,
              graphFxOnHold: sceneElement.graph_fx_on_hold,
            };

            // credits
            const sceneCredits = getScenarioCredits(allCredits, s);

            const creditsComposition = {
              data: getCreditsByRole("composition", sceneCredits),
              hasCredits: false,
              credits: "",
            };
            setCredits(creditsComposition);
            s.composer = creditsComposition.credits;

            const creditsVisual = {
              data: getCreditsByRole("création visuelle", sceneCredits),
              hasCredits: false,
              credits: "",
            };
            setCredits(creditsVisual);
            s.designer = creditsVisual.credits;

            // scene intro
            if (scenarioIntro.length) {
              if (s.scenarioIdentifier == scenarioIntro[0].identifier) {
                sceneIntro = s;
              }
            }

            selectedScenes.push([s.name, s]);
          }
        }
      });
    });

    // add default intro scenes if there is no intro scene created
    // const introScenes = selectedScenes.filter((scene) => scene[0].startsWith(scenarioIntro[0].identifier));
    // if (!introScenes.length) {
    //   selectedScenes.push(["intro-01", intro01]);
    // }

    // add credits scene
    selectedScenes.push(["credits", credits]);

    // console.log("SELECTED SCENES: ", selectedScenes);

    homeScenesList = Object.fromEntries(selectedScenes);
    // console.log("HOMES SCENES LIST: ", homeScenesList);

    return homeScenesList;
  }
  const emptyScenesList: [string, Scene][] = [];
  // emptyScenesList.push(["intro-01", intro01]);
  emptyScenesList.push(["credits", credits]);

  return Object.fromEntries(emptyScenesList);
}

// function getIdentifierHash(identifier: string) {
//   return identifier.split("-").pop() ?? "";
// }

function setIdentifier(scenario: ScenarioApi, scene: SceneApi, isNext: boolean): string {
  const isSingleDigit = scene.order < (isNext ? 9 : 10);
  // let hash = getIdentifierHash(scenario.identifier);
  // hash += "-" + (isSingleDigit ? "0" : "") + String(scene.order + (isNext ? 1 : 0));
  // return hash;
  let id = scenario.identifier;
  id += "-" + (isSingleDigit ? "0" : "") + String(scene.order + (isNext ? 1 : 0));
  return id;
}

function toSeconds(duration: string) {
  if (!duration) duration = "00:00:05";
  const [h, m, s] = duration.split(":").map(Number);
  return h * 3600 + m * 60 + s;
}

export const findImage = (name: string, sizes = [1920, 3840], ext = "png"): string =>
  `${process.env.PUBLIC_URL}/images/${sizes.find((size) => size >= innerWidth)}/${name}.${ext}`;
