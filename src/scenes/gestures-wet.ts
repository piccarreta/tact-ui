import { GestureHandlers } from "./typings";
import { Scene } from "../api/typings";
import { sleep } from "../utils/time";
import { createBrush } from "../utils/canvas";
import { CanvasGrid } from "../utils/grid";
import { TouchNode } from "../utils/touch";

const DEFAULT_PARAMS = {
  radius: 32,
  hardness: 0.5,
  alpha: 1,
};
const TAP_PARAMS = {
  ...DEFAULT_PARAMS,
  radius: 16,
};
const MAX_RADIUS = 120;
const clear = new Image();
const clearCtx = document.createElement("canvas").getContext("2d") as CanvasRenderingContext2D;
clear.crossOrigin = "anonymous";

export default (grid: CanvasGrid, scene?: Scene): GestureHandlers => {
  clearCtx.canvas.width = grid.width;
  clearCtx.canvas.height = grid.height;
  clear.src = scene ? scene.wetImageSrc : "";
  clear.onload = () => clearCtx.drawImage(clear, 0, 0);
  // const palette = document.createElement("canvas").getContext("2d") as CanvasRenderingContext2D;

  // const isAfterTapZigZag = scene?.graphFxAfterTap?.after_tap_fx === 1;

  return {
    onMove: (node, params = DEFAULT_PARAMS) => {
      if (!scene?.graphFxOnMove?.enabled) {
        return;
      }
      const fx = "stick";
      const touch = node.getTouch();
      params.radius = scene.graphFxOnMove.brush_radius;
      params.alpha = scene.graphFxOnMove.accuracy;
      params.hardness = scene.graphFxOnMove.hardness;
      const brush = createBrush({ ...touch, ...params }, clear);
      return { fx, params, touch, brush };
    },
    afterMove: async function* (node, params = DEFAULT_PARAMS) {
      if (!scene?.graphFxAfterMove?.enabled) {
        return;
      }
      if (scene?.graphFxAfterTap?.enabled && scene.graphFxAfterTap.rain_enabled) {
        const isHorizontal = Math.abs(node.getGesture().moveY) < 200;
        if (!isHorizontal) return;
        const dots = node
          .toArray()
          .filter((t) => t.pos === 1 || t.pos % 8 === 0)
          .concat(node.getLast());
        for (const dot of dots) {
          yield* this.afterTap(new TouchNode(dot.getItem()), {
            ...params,
            radius: scene.graphFxAfterMove.brush_radius,
          });
        }
      }
    },
    afterTap: async function* (node, params = TAP_PARAMS) {
      if (!scene?.graphFxAfterTap?.enabled) {
        return;
      }
      params.radius = scene.graphFxAfterTap.brush_radius;

      // if (scene?.graphFxAfterTap?.rain_enabled && isAfterTapZigZag) {
      //   const gesture = node.fallErratically(8);
      //   const fx = opts.fx || "fall";

      //   if (isAfterTapZigZag) {
      //     // ZigZag fx
      //     clear.crossOrigin = "anonymous";
      //     if (scene?.imageSrc) clear.src = scene?.imageSrc;
      //     clearCtx.canvas.width = grid.width;
      //     clearCtx.canvas.height = grid.height;
      //     clear.onload = () => clearCtx.drawImage(clear, 0, 0);
      //   }

      //   for (const renode of gesture.toArray()) {
      //     if (!renode.isLast) await sleep((opts.wait as number) || renode.elapsed);
      //     const touch = renode.getTouch();
      //     yield { fx, params, touch, wait: 15, brush: clearCtx };
      //   }
      // } else if (scene?.graphFxAfterTap?.rain_enabled && !isAfterTapZigZag) {
      //   // Straight fx
      //   const fx = "drip";
      //   const first = node.getTouch();
      //   const gesture = node.rain(grid.height, false);
      //   for (const renode of gesture.toArray()) {
      //     const touch = renode.getTouch();
      //     const brush = createBrush({ ...first, ...params }, grid.image);
      //     if (!renode.isLast) await sleep(20);
      //     yield { fx, params, touch, brush };
      //   }
      // }

      const gesture = node.fallErratically(8);
      const fx = "fall";
      for (const renode of gesture.toArray()) {
        if (!renode.isLast) await sleep(renode.elapsed);
        const touch = renode.getTouch();
        yield { fx, params, touch, wait: 15, brush: clearCtx };
      }
    },
    onHold: (node, params = DEFAULT_PARAMS) => {
      if (!scene?.graphFxOnHold?.enabled) {
        return;
      }
      // const isCircular = scene.graphFxOnHold.shape === 0;

      // if (isCircular) {
      //   const fx = "stick";
      //   const touch = node.getTouch();
      //   const MAX_RADIUS = scene.graphFxOnHold.max_radius;
      //   const displaySpeed = scene.graphFxOnHold.display_speed * 1000;
      //   const radius = Math.min(
      //     (scene.graphFxOnMove ? scene.graphFxOnMove.brush_radius * node.elapsed : 40) / displaySpeed,
      //     MAX_RADIUS
      //   );
      //   palette.canvas.width = radius * 2;
      //   palette.canvas.height = radius * 2;
      //   palette.arc(radius, radius, radius, 0, Math.PI * 2);
      //   palette.clip();
      //   palette.fillStyle = pickColor(touch, grid.image);
      //   palette.fillRect(0, 0, radius * 2, radius * 2);
      //   return {
      //     fx,
      //     touch,
      //     brush: palette,
      //     params,
      //   };
      // } else {
      //   const fx = "stick";
      //   const touch = node.getTouch();
      //   const MAX_RADIUS = 80;
      //   const displaySpeed = scene.graphFxOnHold.display_speed * 1000;
      //   const radius = Math.min(
      //     (scene.graphFxOnMove ? scene.graphFxOnMove.brush_radius * node.elapsed : 40) / displaySpeed,
      //     MAX_RADIUS
      //   );
      //   const brush = document.createElement("canvas").getContext("2d") as CanvasRenderingContext2D;
      //   const [width, height] = Array(2).fill(radius * 2);
      //   const MAX_WIDTH = scene.graphFxOnHold.max_width;
      //   const MAX_HEIGHT = scene.graphFxOnHold.max_height;
      //   brush.canvas.width = Math.min(width, MAX_WIDTH);
      //   brush.canvas.height = Math.min(height, MAX_HEIGHT);
      //   brush.fillStyle = pickColor(touch, grid.image);
      //   brush.fillRect(0, 0, width, height);
      //   return { fx, touch, brush, params: { ...params, radius } };
      // }
      const fx = "stick";
      const touch = node.getTouch();
      const displaySpeed = scene.graphFxOnHold.display_speed * 1000;
      const radius = Math.min(
        (scene.graphFxOnMove ? scene.graphFxOnMove.brush_radius * node.elapsed : 40) / displaySpeed,
        MAX_RADIUS
      );
      const brush = createBrush({ ...touch, ...params, radius }, clear);
      return { fx, touch, brush, params };
    },
  } as GestureHandlers;
};
