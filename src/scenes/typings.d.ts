import { TouchNode, ExtendedTouch } from "../utils/touch";

type Gestures = "onMove" | "onTap" | "onHold";
type AfterGestures = "afterMove" | "afterTap" | "afterHold";
export type Instruction = {
  params: { radius: number; hardness: number; alpha: number };
  touch: ExtendedTouch;
  brush?: CanvasRenderingContext2D;
};
type FXHandler = Record<
  Gestures,
  (
    node: TouchNode,
    params?: Instruction["params"],
    opts?: Record<string, number | string | boolean>
  ) => Instruction & { fx: string }
>;
type AfterFXHandler = Record<
  AfterGestures,
  (
    node: TouchNode,
    params?: Instruction["params"],
    opts?: Record<string, number | string | boolean>
  ) => AsyncGenerator<Instruction & { fx: string }>
>;

export type GestureHandlers = FXHandler & AfterFXHandler & { cleanup?: () => void };
