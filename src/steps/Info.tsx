import React, { useCallback } from "react";
import { FormattedMessage } from "react-intl";
import { useHistory, useParams } from "react-router-dom";

import { useRecoilValue } from "recoil";
import Overlay, { SlotComponent } from "../components/Overlay";
import Controls from "../components/Controls";
import Button from "../components/Button";
import { getInfosFromApi } from "../state";
import { getSceneIntro } from "../scenes";
import styles from "../styles/steps.module.css";

const InfosStep: React.FC = () => {
  const infos = useRecoilValue(getInfosFromApi);
  if (!infos.length) {
    infos.push({
      id: 0,
      name: "",
      artistic_direction: "",
      coproduction: "",
      cultural_engineering: "",
      design: "",
      furniture_design: "",
      furniture_realisation: "",
      production: "",
      production_manager: "",
      rim: "",
      programming: "",
    });
  }
  // console.log("INFOS:", infos);

  const history = useHistory();
  const { scene } = useParams<{ scene: string }>();
  let sceneIntro;
  let sceneIntroIdentifier = "";
  if (scene === undefined) {
    sceneIntro = getSceneIntro();
    if (sceneIntro) {
      sceneIntroIdentifier = sceneIntro.name;
    }
  }
  if (sceneIntro === undefined) {
    sceneIntro = getSceneIntro();
    if (sceneIntro) {
      sceneIntroIdentifier = sceneIntro.name;
    }
  }
  const onClose = useCallback(
    (cb?: React.MouseEventHandler) => (e: React.MouseEvent) => {
      if (typeof cb === "function") {
        cb(e);
        history.push(`/${scene ?? sceneIntroIdentifier}/play`, { showIntro: true });
        // history.go(0);
      } else {
        history.push(`/${scene ?? sceneIntroIdentifier}/play`);
      }
    },
    [history, sceneIntroIdentifier, scene]
  );
  const onTouchClose = useCallback(
    (cb?: React.TouchEventHandler) => (e: React.TouchEvent) => {
      if (typeof cb === "function") {
        cb(e);
        history.push(`/${scene ?? sceneIntroIdentifier}/play`, { showIntro: true });
        history.go(0);
      } else {
        history.push(`/${scene ?? sceneIntroIdentifier}/play`);
      }
    },
    [history, sceneIntroIdentifier, scene]
  );
  const footerElement = useCallback<SlotComponent>(
    ({ close }) => (
      <Button
        bordered
        size="large"
        color="white"
        onClick={onClose(close as React.MouseEventHandler)}
        onTouchStart={onTouchClose(close as React.TouchEventHandler)}
      >
        <FormattedMessage id="home.play" />
      </Button>
    ),
    [onClose, onTouchClose]
  );

  return (
    <>
      <Overlay onClose={onClose()} footer={sceneIntro ? footerElement : null}>
        <div className={styles.credits}>
          <div>
            <strong>
              <FormattedMessage id="info.intro" tagName="h3" />
            </strong>
            <FormattedMessage id="info.message1" tagName="p" />
            <div style={{ textAlign: "center", marginBottom: "1rem" }}>
              <img src={`${process.env.PUBLIC_URL}/gestures.png`} />
            </div>
            {/* <div>
              <FormattedMessage id="info.creation" tagName="u" />
              <ul style={{ listStyleType: "none" }}>
                <li>
                  Georges Aperghis, <i>Sur les quais</i>
                </li>
                <li>
                  Romain Barthélémy, <i>Canal Ciment</i>
                </li>
                <li>
                  Fabien Bourlier, <i>Network</i>
                </li>
                <li>
                  Didem Coskunseven, <i>Locus Solus</i>
                </li>
                <li>
                  Lundja Medjoub, <i>Massy Opéra</i>
                </li>
              </ul>
              <FormattedMessage id="credits.sound" tagName="p" values={{ person: <b>Romain Barthélémy</b> }} />
              <div className={styles.info_design}>
                <FormattedMessage id="info.design" tagName="u" />
              </div>
              <FormattedMessage
                id="credits.design"
                tagName="p"
                values={{
                  person: (
                    <b>
                      Zoé Aegerter, Philippe Barbosa, Romain Barthélémy, Quentin Chevrier, Sergio Garcia, Mathieu
                      Ghezzi, Lundja Medjoub
                    </b>
                  ),
                }}
              />
            </div> */}
          </div>

          <div>
            <div className={styles.authors}>
              <p>
                <strong>TACT - Centre Pompidou</strong>
              </p>
              <p>
                <FormattedMessage id="info.description" tagName="strong" />
              </p>
              <p style={{ marginBottom: 25 }}>
                <FormattedMessage id="info.subtitle" />
              </p>
              <div style={{ marginBottom: 25 }}>
                <FormattedMessage
                  id="credits.artistic"
                  tagName="p"
                  values={{ person: <b>{infos[0].artistic_direction}</b> }}
                />
              </div>
              <div style={{ marginBottom: 25 }}>
                <FormattedMessage
                  id="credits.production"
                  tagName="p"
                  values={{ person: <b>{infos[0].production}</b> }}
                />
                <FormattedMessage id="credits.coprod" values={{ person: <b>{infos[0].coproduction}</b> }} />
              </div>
              <div style={{ marginBottom: 25 }}>
                <FormattedMessage id="credits.rim" tagName="p" values={{ person: <b>{infos[0].rim}</b> }} />
              </div>
              <div style={{ marginBottom: 25 }}>
                <FormattedMessage id="credits.design" tagName="p" values={{ person: <b>{infos[0].design}</b> }} />
              </div>
              <div style={{ marginBottom: 25 }}>
                <FormattedMessage
                  id="credits.furniture.design"
                  tagName="p"
                  values={{ person: <b>{infos[0].furniture_design}</b> }}
                />
                <FormattedMessage
                  id="credits.furniture.build"
                  values={{ person: <b>{infos[0].furniture_realisation}</b> }}
                />
              </div>
              <div style={{ marginBottom: 25 }}>
                <FormattedMessage id="credits.code" tagName="p" values={{ person: <b>{infos[0].programming}</b> }} />
              </div>
              <div style={{ marginBottom: 25 }}>
                <FormattedMessage
                  id="credits.chief"
                  tagName="p"
                  values={{ person: <b>{infos[0].cultural_engineering}</b> }}
                />
                <FormattedMessage id="credits.chief.assist" values={{ person: <b>{infos[0].production_manager}</b> }} />
              </div>
              <FormattedMessage id="info.subtitle2" />
            </div>
          </div>
        </div>
      </Overlay>
      <Controls />
    </>
  );
};

export default InfosStep;
