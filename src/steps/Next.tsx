import { useCallback, useEffect, useMemo, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { RecoilState, useRecoilState, useSetRecoilState, useRecoilValue } from "recoil";
import { filter, fromEvent, of, Subscription, takeUntil, timer } from "rxjs";
import { useXebra } from "../hooks/xebra";
import { getSceneFromList, getScenesFromApi, getSceneIntro, getSceneInfoFromUrl, getScenarioDuration } from "../scenes";
import { Scene } from "../api/typings";
import { isPlaying, sceneState } from "../state";
import { fade$ } from "../utils/time";

import styles from "../styles/steps.module.css";

const isDev = false;
// const isDev = process.env.NODE_ENV !== "production";

const next$ = fromEvent<KeyboardEvent>(document, "keydown").pipe(
  filter((event) => event.altKey && event.ctrlKey),
  filter((event) => event.key === "n")
);

function fadeCanvas(canvas: HTMLCanvasElement, alpha: number, transitionColor: number) {
  const canvasContainer = document.getElementById("canvasContainer") as HTMLElement;
  if (transitionColor == 0) {
    canvasContainer.style.backgroundColor = "black";
  } else {
    canvasContainer.style.backgroundColor = "white";
  }

  canvas.style.opacity = (1 - alpha).toString();
}

const fader = (out: number, complete: () => void, transitionColor?: number): Subscription => {
  const canvas = document.getElementById("foreground") as HTMLCanvasElement;
  return (!canvas || !out ? of(0) : fade$(out)).subscribe({
    next: (alpha) => fadeCanvas(canvas, alpha, transitionColor ?? 0),
    complete,
  });
};

const useDelayedRecoilState = (state: RecoilState<Scene>) => {
  const [value, dispatch] = useRecoilState(state);
  const delayedDispatch: typeof dispatch = (nextVal) =>
    fader(value.transitionOut ?? 0, () => dispatch(nextVal), value.transitionColor ?? 0);
  return [value, delayedDispatch] as const;
};

const NextButton: React.FC = () => {
  const history = useHistory();
  const { scene: creation = "" } = useParams<{ scene: string }>();
  const { sendMessage } = useXebra();

  const scenesList = getScenesFromApi();
  const sceneIntro = getSceneIntro();

  const [show, setShow] = useState(isDev);
  const [current, next] = useDelayedRecoilState(sceneState);
  const playing = useRecoilValue(isPlaying);

  let isCreditScene = false;
  let creditDuration: number;
  if (current.name === "credits") {
    isCreditScene = true;
    const sceneInfo = getSceneInfoFromUrl(window.location.href);
    if (sceneInfo.scenario != "") {
      creditDuration = getScenarioDuration(sceneInfo.scenario) as number;
    }
  }

  const { name, transitionOut = 0, transitionColor, duration, next: imNext } = useMemo(() => current, [current]);

  const setScene = useSetRecoilState(sceneState);

  const handleClick = useCallback(() => {
    setShow(isDev);
    if (name) sendMessage("stop", { name: name === "credits" ? `credits-${creation}` : name });

    // async function nextScene(imNext: string) {
    //   history.push(`/${imNext}/play`, { showIntro: false });
    //   // await new Promise((r) => setTimeout(r, 2000));
    //   setScene(getSceneFromList(imNext, scenesList));
    // }

    function nextCredits(name: string, imNext: string) {
      history.push(`/${name}/play`, { showIntro: false });
      setScene(getSceneFromList(imNext, scenesList));
    }

    // Next scene click event
    if (sceneIntro && name.startsWith(sceneIntro.scenarioIdentifier)) {
      fader(transitionOut, () => history.push("/"), 1);
    } else if (imNext) {
      if (sceneIntro) {
        if (!imNext.startsWith(sceneIntro.scenarioIdentifier) && !imNext.startsWith("credits")) {
          next(getSceneFromList(imNext, scenesList));
          history.push(`/${imNext}/play`, { showIntro: false });

          // fader(transitionOut, () => nextScene(imNext), transitionColor);
        }
      } else {
        if (!imNext.startsWith("credits")) {
          next(getSceneFromList(imNext, scenesList));
          history.push(`/${imNext}/play`, { showIntro: false });

          // fader(transitionOut, () => nextScene(imNext), transitionColor);
        }
      }

      if (imNext.startsWith("credits")) {
        fader(transitionOut, () => nextCredits(name, imNext), transitionColor);
      }
    } else {
      history.push("/");
    }
  }, [
    name,
    sendMessage,
    creation,
    sceneIntro,
    imNext,
    history,
    setScene,
    scenesList,
    transitionOut,
    next,
    transitionColor,
  ]);

  const handleTouchStart = useCallback(() => {
    setShow(isDev);
    if (name) sendMessage("stop", { name: name === "credits" ? `credits-${creation}` : name });

    // function nextScene(imNext: string) {
    //   history.push(`/${imNext}/play`, { showIntro: false });
    //   setScene(getSceneFromList(imNext, scenesList));
    // }

    function nextCredits(name: string, imNext: string) {
      history.push(`/${name}/play`, { showIntro: false });
      setScene(getSceneFromList(imNext, scenesList));
    }

    // Next scene touch event
    if (name.startsWith(sceneIntro.scenarioIdentifier)) {
      fader(transitionOut, () => history.push("/"), 1);
    } else if (imNext) {
      if (!imNext.startsWith(sceneIntro.scenarioIdentifier) && !imNext.startsWith("credits")) {
        next(getSceneFromList(imNext, scenesList));
        history.push(`/${imNext}/play`, { showIntro: false });

        // fader(transitionOut, () => nextScene(imNext), transitionColor);
      }

      if (imNext.startsWith("credits")) {
        fader(transitionOut, () => nextCredits(name, imNext), transitionColor);
      }
    } else {
      history.push("/");
    }
  }, [
    name,
    sendMessage,
    creation,
    sceneIntro,
    imNext,
    history,
    setScene,
    scenesList,
    transitionOut,
    next,
    transitionColor,
  ]);

  useEffect(() => {
    setShow(isDev);
    if (!playing) return undefined;

    const sub = timer((isCreditScene ? creditDuration : duration) * 1000)
      .pipe(takeUntil(next$))
      .subscribe(() => {
        if (name === "credits") {
          history.push("/");
        } else {
          setShow(true);
        }
      });
    return () => sub.unsubscribe();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [playing, name, duration, history]);

  return show ? (
    <button onClick={handleClick} onTouchStart={handleTouchStart} className={styles.nextBtn}>
      Next ➜
    </button>
  ) : null;
};

export default NextButton;
