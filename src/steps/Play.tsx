import React, { useCallback, useEffect, useMemo } from "react";
import { FormattedMessage } from "react-intl";
import { useLocation, useParams } from "react-router";
import { useRecoilState, useRecoilValue } from "recoil";

import Overlay, { SlotComponent } from "../components/Overlay";
import Button from "../components/Button";
import { InfoButton } from "../components/Button";
import Controls from "../components/Controls";
import { playerState, sceneState } from "../state";
import styles from "../styles/steps.module.css";
import { useXebra } from "../hooks/xebra";

const PlayStep: React.FC<{ onHide?: () => void }> = ({ onHide }) => {
  const location = useLocation<{ showIntro: boolean }>();
  const { scene: creation } = useParams<{ scene: string }>();
  const { sendMessage } = useXebra();
  const [player, setPlayer] = useRecoilState(playerState);
  const scene = useRecoilValue(sceneState);
  const { showIntro } = useMemo(
    () => (player === "stopped" && location.state) || { showIntro: false },
    [location.state, player]
  );

  const onClose = useCallback(
    () => () => {
      if (typeof onHide === "function") onHide();
      setPlayer("playing");
    },
    [setPlayer, onHide]
  );

  const onPlayButtonSubmit = useCallback(
    () => () => {
      if (typeof onHide === "function") onHide();
      if (player != "playing") setPlayer("playing");
    },
    [player, setPlayer, onHide]
  );

  const footerElement = useCallback<SlotComponent>(
    () => (
      <div
        style={{
          width: "100%",
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <InfoButton rounded />
        <Button bordered size="large" color="white" onClick={onPlayButtonSubmit()} onTouchStart={onPlayButtonSubmit()}>
          <FormattedMessage id="home.play" />
        </Button>
        <InfoButton style={{ visibility: "hidden" }} rounded />
      </div>
    ),
    [onPlayButtonSubmit]
  );

  useEffect(() => {
    if (!showIntro && player === "stopped") setPlayer("playing");
  }, [showIntro, player, setPlayer]);

  useEffect(() => {
    if (player === "playing") {
      const { name, scenarioIdentifier, order } = scene;
      // Send scene messages to Max
      // send only scenario identifier (string) if first scene
      // then send scenario identifier + scene order (int)
      if (name) {
        // console.log("/////////////////////////////");
        // console.log("scenario: %s scene: %s", scenarioIdentifier, order);
        // console.log("/////////////////////////////");

        if (scenarioIdentifier === "credits") {
          console.log("SEND MESSAGE: ", scenarioIdentifier);
          sendMessage("begin", {
            scene: "credits",
          });
        }

        if (scenarioIdentifier !== "credits" && order) {
          if (order === 1) {
            if (location.pathname.split("/")[1].endsWith("01")) {
              console.log("SEND MESSAGE: begin" + " SCENARIO: " + scenarioIdentifier + ".json");
              sendMessage("begin", {
                scenario: scenarioIdentifier + ".json",
                scene: 1,
              });
            }
          } else {
            console.log("SEND MESSAGE: begin" + " SCENARIO: " + scenarioIdentifier + ".json" + " SCENE: " + order);
            sendMessage("begin", {
              scenario: scenarioIdentifier + ".json",
              scene: order,
            });
          }
        }
      }
    }
  }, [player, scene, creation, sendMessage, location.pathname]);

  return (
    <>
      {showIntro && (
        <Overlay onClose={onClose} footer={footerElement}>
          <FormattedMessage id="play.help" tagName="p" />
          <div className={styles.play}>
            <div>
              <img src={`${process.env.PUBLIC_URL}/icon-touch.png`} alt="Touch" />
              <FormattedMessage id="play.touch" />
            </div>
            <div>
              <img src={`${process.env.PUBLIC_URL}/icon-hold.png`} alt="Hold" />
              <FormattedMessage id="play.hold" />
            </div>
            <div>
              <img src={`${process.env.PUBLIC_URL}/icon-draw.png`} alt="Draw" />
              <FormattedMessage id="play.draw" />
            </div>
          </div>
        </Overlay>
      )}
      <Controls />
    </>
  );
};

export default PlayStep;
