import React, { useCallback } from "react";
import { FormattedMessage } from "react-intl";
import { useHistory } from "react-router-dom";
import { useSetRecoilState } from "recoil";
import Controls from "../components/Controls";
import { sceneState } from "../state";
import { getScenesFromApi, getScene, getSceneIntro } from "../scenes";
import { Scene } from "../api/typings";

import styles from "../styles/steps.module.css";

const Home: React.FC = () => {
  const history = useHistory();
  const setScene = useSetRecoilState(sceneState);

  const scenesList = getScenesFromApi();
  // console.log(scenesList);
  const sceneIntro = getSceneIntro();

  // API REFACTOR
  const handleClick = useCallback(
    (key: keyof typeof scenesList) => {
      setScene(getScene(key));
      history.push(`/${key}/play`, { showIntro: false });
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [history, scenesList, setScene]
  );

  const handleClickInfo = () => {
    history.push(`/info`);
  };

  const homeScenes: [string, Scene][] = [];
  if (Object.entries(scenesList).length) {
    for (let i = 0; i < Object.entries(scenesList).length; i++) {
      if (sceneIntro) {
        if (
          !Object.entries(scenesList)[i][0].startsWith(sceneIntro.scenarioIdentifier) &&
          !Object.entries(scenesList)[i][0].startsWith("credits")
        ) {
          if (Object.entries(scenesList)[i][1].order === 1) {
            homeScenes.push([Object.entries(scenesList)[i][0], Object.entries(scenesList)[i][1]]);
          }
        }
      } else {
        if (!Object.entries(scenesList)[i][0].startsWith("credits")) {
          if (Object.entries(scenesList)[i][1].order === 1) {
            homeScenes.push([Object.entries(scenesList)[i][0], Object.entries(scenesList)[i][1]]);
          }
        }
      }
    }
  }

  const evenScenes = [];
  const oddScenes = [];

  for (let i = 0; i < homeScenes.length; i++) {
    if (sceneIntro) {
      // exclude intro scenario from home cards
      if (!homeScenes[i][0].startsWith(sceneIntro.scenarioIdentifier)) {
        if (i % 2 === 0) {
          evenScenes.push(homeScenes[i]);
        } else {
          oddScenes.push(homeScenes[i]);
        }
      }
    } else {
      if (i % 2 === 0) {
        evenScenes.push(homeScenes[i]);
      } else {
        oddScenes.push(homeScenes[i]);
      }
    }
  }

  return (
    <div className={styles.home_container}>
      <div className={styles.scenes_cards + " " + styles.home}>
        <div className={"home-scene-card " + styles.scene + " " + styles.info_card}>
          <div className={styles.scene_card_content}>
            <h2>{"LET'S PLAY"}</h2>
            <p>{"À VOUS DE JOUER !"}</p>
            <button onClick={() => handleClickInfo()} onTouchStart={() => handleClickInfo()}>
              Info
            </button>
          </div>
        </div>
        {evenScenes.map(([name, scene]) => (
          <div
            onClick={() => handleClick(name)}
            onTouchStart={() => handleClick(name)}
            key={name}
            className={"home-scene-card " + styles.scene}
            style={{
              backgroundImage: `url('${
                scene.scenario?.cover_image
                  ? scene.scenario?.cover_image.file
                  : process.env.PUBLIC_URL + "/images/1920/default-cover.png"
              }')`,
            }}
          >
            <div className={styles.scene_card_content}>
              <h1>{scene.scenario?.name}</h1>
              <div style={{ textAlign: "center" }}>
                <p>
                  <FormattedMessage id="home.composition" /> - {scene.composer}
                </p>
                <p>
                  <FormattedMessage id="home.design" /> - {scene.designer}
                </p>
              </div>
              <button>
                <FormattedMessage id="home.play" />
              </button>
            </div>
          </div>
        ))}
      </div>
      <div className={styles.scenes_cards + " " + styles.home}>
        {oddScenes.map(([name, scene]) => (
          <div
            onClick={() => handleClick(name)}
            onTouchStart={() => handleClick(name)}
            key={name}
            className={"home-scene-card " + styles.scene}
            style={{
              backgroundImage: `url('${
                scene.scenario?.cover_image
                  ? scene.scenario?.cover_image.file
                  : process.env.PUBLIC_URL + "/images/1920/default-cover.png"
              }')`,
            }}
          >
            <div className={styles.scene_card_content}>
              <h1>{scene.scenario?.name}</h1>
              <div style={{ textAlign: "center" }}>
                <p>
                  <FormattedMessage id="home.composition" /> - {scene.composer}
                </p>
                <p>
                  <FormattedMessage id="home.design" /> - {scene.designer}
                </p>
              </div>
              <button>
                <FormattedMessage id="home.play" />
              </button>
            </div>
          </div>
        ))}
        <Controls />
      </div>
    </div>
  );
};

export default Home;
