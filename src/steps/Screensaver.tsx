import React, { useEffect } from "react";
import { useLocation } from "react-router-dom";
import { useHardRefresh } from "../hooks/location";
import { getSceneIntro } from "../scenes";

const ScreenSaverStep: React.FC = () => {
  let sceneIntroIdentifier = "";
  const sceneIntro = getSceneIntro();
  if (sceneIntro) {
    sceneIntroIdentifier = sceneIntro.name;
  }
  const refresh = useHardRefresh(process.env.PUBLIC_URL + "/tact-ui/" + sceneIntroIdentifier + "/play");
  const location = useLocation();
  useEffect(() => {
    if (location.pathname !== process.env.PUBLIC_URL + "/tact-ui/" + sceneIntroIdentifier + "/play") {
      refresh();
    }
  }, [location, sceneIntroIdentifier, refresh]);

  return null;
};

export default ScreenSaverStep;
