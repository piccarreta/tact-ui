import React, { useCallback, useEffect, useMemo, useRef } from "react";
import { useParams } from "react-router-dom";
import { useGesture, UserGestureConfig } from "@use-gesture/react";
import { useRecoilValue } from "recoil";

import CreditsStep from "./Credits";
import { useCanvas } from "../hooks/canvas";
import { useRegistry, TouchHandler } from "../hooks/registry";
import { useVFX } from "../hooks/vfx";
import { GESTURE_ENUM } from "../hooks/xebra";
import { useRafLoop } from "../hooks/raf";
import { getCoordForCanvas } from "../utils/canvas";
import { TouchItem, TouchNode, MAX_TOUCHES_NUM } from "../utils/touch";
import { Instruction } from "../scenes/typings";
import { hasNext } from "../state";
import { getScenesFromApi, getSceneFromList, getSceneIntro } from "../scenes";

const Canvas: React.FC = () => {
  let { scene } = useParams<{ scene: string }>();
  const scenesList = getScenesFromApi();
  const sceneData = getSceneFromList(scene, scenesList);
  scene = scene.slice(0, -3);
  const sceneIntro = getSceneIntro();
  const [mainCanvas, grid] = useCanvas({ scene });
  const [vfx, gestures] = useVFX(grid, scene, sceneData);
  const { read, write, clear } = useRegistry();
  // console.log("hasNext(scene): ", hasNext(scene));
  const isInteractive = useRecoilValue(hasNext(scene));
  // console.log("isInteractive: ", isInteractive);
  const options = useMemo<UserGestureConfig>(
    () => ({
      transform: ([clientX, clientY]) => {
        const canvas = mainCanvas.current as HTMLCanvasElement;
        if (!canvas) return [clientX, clientY];
        const { x, y } = getCoordForCanvas({ clientX, clientY }, canvas);
        return [x, y];
      },
      target: mainCanvas,
      eventOptions: { passive: false, preventDefault: true, pointer: { touch: true } },
      enabled: true,
      // preventDefault: true,
      // pointer: { touch: true },
    }),
    [mainCanvas]
  );

  // Max fingers warning
  const warningElement = useRef(document.getElementById("play-overlay-warning"));
  const showWarning = useRef(false);
  const warningReadyToHide = useRef(false);
  const warningDelay = 1000;

  useEffect(() => {
    const interval = setInterval(() => {
      if (!warningElement.current) {
        warningElement.current = document.getElementById("play-overlay-warning");
      }

      if (warningReadyToHide.current && showWarning.current && warningElement.current?.style) {
        warningElement.current.style.visibility = "hidden";
        warningElement.current.style.opacity = "0";
        showWarning.current = false;
        warningReadyToHide.current = false;
      }
    }, warningDelay);

    return () => clearInterval(interval);
  }, []);

  const [startLoop, stopLoop, isActive] = useRafLoop((time, id) => {
    const node = read(id);
    if (!node || typeof gestures?.onHold !== "function" || !sceneData.graphFxOnHold?.enabled) return;
    const elapsed = Math.round(time - node.t);
    if (elapsed <= TouchNode.SPEED_THRESHOLD) return;
    const { fx, ...instruction } = gestures.onHold(node);
    vfx[fx as keyof typeof vfx](instruction as Instruction);
  });

  const activeMovingNode = useCallback<(t: TouchItem) => boolean>(
    (touch) => {
      const { h: isHolding } = touch;
      return !isHolding && !grid?.isMarked(touch);
    },
    [grid]
  );

  const onStart = useCallback<TouchHandler>(
    (node) => {
      if (!isActive(node.id)) startLoop(node.id);
    },
    [isActive, startLoop]
  );
  const onMove = useCallback<TouchHandler>(
    (node) => {
      if (node.elapsed < 2 || typeof gestures?.onMove !== "function" || !sceneData.graphFxOnMove?.enabled) return;
      stopLoop(node.id);
      const { fx, ...instruction } = gestures.onMove(node);
      vfx[fx as keyof typeof vfx](instruction as Instruction);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [gestures, stopLoop, vfx]
  );
  const onEnd = useCallback<TouchHandler>(
    async (node) => {
      const { id, isTap = false, move = 0 } = node.getGesture();
      const isMoving = !isTap && Math.abs(move) > TouchNode.TAP_THRESHOLD;
      const isHolding = !isTap && !isMoving && isActive(id);

      stopLoop(id);

      if (isTap && !isHolding) {
        if (typeof gestures?.onTap === "function") {
          const { fx, ...instruction } = gestures.onTap(node);
          await vfx[fx as keyof typeof vfx](instruction as Instruction);
        }
        if (typeof gestures?.afterTap === "function") {
          for await (const { fx, ...inst } of gestures.afterTap(node)) {
            await vfx[fx as keyof typeof vfx](inst as Instruction);
          }
        }
      }

      clear(id);

      // console.log(id + " isMoving: " + isMoving + " isHolding: " + isHolding + " isTap: " + isTap);

      // After effects (closed on new gesture)
      if (isHolding && typeof gestures?.afterHold === "function") {
        for await (const { fx, ...instruction } of gestures.afterHold(node)) {
          if (read(id)) break;
          await vfx[fx as keyof typeof vfx](instruction as Instruction);
        }
      } else if (isMoving && typeof gestures?.afterMove === "function") {
        for await (const { fx, ...instruction } of gestures.afterMove(node)) {
          if (read(id)) break;
          await vfx[fx as keyof typeof vfx](instruction as Instruction);
        }
      }
    },
    [isActive, stopLoop, clear, gestures, vfx, read]
  );

  useEffect(
    () => () => {
      if (typeof gestures?.cleanup === "function") gestures.cleanup();
    },
    [gestures]
  );

  useGesture<{
    onTouchStart: TouchEvent;
    onTouchMove: TouchEvent;
    onTouchEnd: TouchEvent;
    onPointerDown: PointerEvent;
    onPointerMove: PointerEvent;
    onPointerUp: PointerEvent;
  }>(
    {
      onTouchStart: ({ event }) => {
        write(event, GESTURE_ENUM.pointerdown).forEach(onStart);

        const touchesNum = event.targetTouches.length;
        if (touchesNum > MAX_TOUCHES_NUM) {
          if (!showWarning.current && warningElement.current?.style) {
            warningElement.current.style.visibility = "visible";
            warningElement.current.style.opacity = "1";
            showWarning.current = true;
          }
        }
      },
      onTouchMove: ({ event }) => {
        write(event, GESTURE_ENUM.pointermove).filter(activeMovingNode).forEach(onMove);
      },
      onTouchEnd: ({ event }) => {
        write(event, GESTURE_ENUM.pointerup).filter(activeMovingNode).forEach(onEnd);

        const touchesNum = event.targetTouches.length;
        if (touchesNum <= MAX_TOUCHES_NUM) {
          if (showWarning.current) {
            // warningElement.current.style.visibility = "hidden";
            // warningElement.current.style.opacity = "0";
            warningReadyToHide.current = true;
          }
        }
      },
      onPointerDown: ({ event }) => {
        write(event, GESTURE_ENUM.pointerdown);
      },
      onPointerMove: ({ event }) => {
        write(event, GESTURE_ENUM.pointermove).filter(activeMovingNode).forEach(onMove);
      },
      onPointerUp: ({ event }) => {
        write(event, GESTURE_ENUM.pointerup).filter(activeMovingNode).forEach(onEnd);
      },
    },
    options
  );

  return (
    <div
      id="canvasContainer"
      style={{
        width: "100%",
        backgroundColor: sceneIntro ? (scene.startsWith(sceneIntro.scenarioIdentifier) ? "white" : "black") : "black",
      }}
    >
      {isInteractive || scene.startsWith(sceneIntro.scenarioIdentifier) ? (
        <canvas id="foreground" ref={mainCanvas} />
      ) : (
        <CreditsStep />
      )}
    </div>
  );
};

export default Canvas;
