import React, { useEffect, useMemo } from "react";
import { FormattedMessage } from "react-intl";
import { useParams } from "react-router-dom";
import { useRafState } from "react-use";
import { useRecoilValue } from "recoil";
import LogoIrcam from "./LogoIrcam";
import { getScene } from "../scenes";
import { getCreditsFromApi } from "../state";
import { Scene, CreditApi, Credit } from "../api/typings";
import styles from "../styles/credits.module.css";

export function getScenarioCredits(credits: CreditApi[], scene: Scene): CreditApi[] {
  const selectedCredits: CreditApi[] = credits.filter(
    (credit) => credit.scenario.identifier === scene.scenario?.identifier
  );
  return selectedCredits;
}

export function getCreditsByRole(role: string, credits: CreditApi[]): CreditApi[] {
  const selectedRoles = credits.filter((credit) => credit.role.name === role);
  return selectedRoles;
}

export function setCredits(creditsObject: Credit): void {
  if (creditsObject.data.length) {
    creditsObject.hasCredits = true;
    if (creditsObject.data.length > 1) {
      creditsObject.data.forEach((credit: CreditApi) => {
        creditsObject.credits += (credit.first_name ?? "") + " " + (credit.last_name ?? "") + ", ";
      });
    } else {
      creditsObject.credits += (creditsObject.data[0].first_name ?? "") + " " + (creditsObject.data[0].last_name ?? "");
    }
  }
}

const CreditsStep: React.FC = () => {
  const { scene: sceneName } = useParams<{ scene: string }>();
  // console.log("sceneName: ", sceneName);
  const scene = getScene(sceneName);
  const credits = getScenarioCredits(useRecoilValue(getCreditsFromApi), scene);
  // console.log("CREDITS:", credits);
  const [opacity, setOpacity] = useRafState(0);
  const backgroundColor = useMemo(() => `rgba(0, 0, 0, ${opacity.toFixed(1)})`, [opacity]);

  // scenario
  const creditsScenario = {
    data: getCreditsByRole("scenario", credits),
    hasCredits: false,
    credits: "",
  };
  setCredits(creditsScenario);

  // composition
  const creditsComposition = {
    data: getCreditsByRole("composition", credits),
    hasCredits: false,
    credits: "",
  };
  setCredits(creditsComposition);

  // visual
  const creditsVisual = {
    data: getCreditsByRole("création visuelle", credits),
    hasCredits: false,
    credits: "",
  };
  setCredits(creditsVisual);

  // photography
  const creditsPhotography = {
    data: getCreditsByRole("photographie", credits),
    hasCredits: false,
    credits: "",
  };
  setCredits(creditsPhotography);

  // composition assistant
  const creditsCompositionAssistant = {
    data: getCreditsByRole("assistant composition", credits),
    hasCredits: false,
    credits: "",
  };
  setCredits(creditsCompositionAssistant);

  // sound
  const creditsSound = {
    data: getCreditsByRole("sound design", credits),
    hasCredits: false,
    credits: "",
  };
  setCredits(creditsSound);

  // mix
  const creditsMix = {
    data: getCreditsByRole("mixage", credits),
    hasCredits: false,
    credits: "",
  };
  setCredits(creditsMix);

  // acknowledgements
  const creditsAcknowledgements = {
    data: getCreditsByRole("remerciements", credits),
    hasCredits: false,
    credits: "",
  };
  setCredits(creditsAcknowledgements);

  useEffect(() => {
    if (opacity >= 1) return;
    setOpacity((value) => value + 0.01);
  }, [opacity, setOpacity]);

  return (
    <div style={{ backgroundColor }} className={styles.credits}>
      <h1>{scene.scenario?.name}</h1>

      <div className={styles.individuals}>
        <div>
          {creditsScenario.hasCredits && (
            <div>
              <FormattedMessage id="credits.scenario" values={{ person: creditsScenario.credits }} tagName="p" />
            </div>
          )}
        </div>

        <div>
          {creditsComposition.hasCredits && (
            <FormattedMessage
              id="credits.composition.creator"
              values={{ person: creditsComposition.credits }}
              tagName="p"
            />
          )}
          {creditsVisual.hasCredits && (
            <FormattedMessage id="credits.design" values={{ person: creditsVisual.credits }} tagName="p" />
          )}
        </div>

        <div>
          {creditsPhotography.hasCredits && (
            <div>
              <FormattedMessage id="credits.photo" values={{ person: creditsPhotography.credits }} tagName="p" />
            </div>
          )}
        </div>

        <div>
          {creditsCompositionAssistant.hasCredits && (
            <div>
              <FormattedMessage
                id="credits.composition"
                values={{ person: creditsCompositionAssistant.credits }}
                tagName="p"
              />
            </div>
          )}
        </div>

        <div>
          {creditsSound.hasCredits && (
            <div>
              <FormattedMessage id="credits.sound" values={{ person: creditsSound.credits }} tagName="p" />
            </div>
          )}
        </div>

        <div>
          {creditsMix.hasCredits && (
            <div>
              <FormattedMessage id="credits.mix" values={{ person: creditsMix.credits }} tagName="p" />
            </div>
          )}
        </div>

        <div className={styles.acknowledgements}>
          {creditsAcknowledgements.hasCredits && (
            <div>
              <FormattedMessage
                id="credits.acknowledgements"
                values={{ person: creditsAcknowledgements.credits }}
                tagName="p"
              />
            </div>
          )}
        </div>
      </div>

      <div>
        <LogoIrcam />
      </div>
    </div>
  );
};

export default CreditsStep;
