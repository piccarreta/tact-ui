import React, { useCallback } from "react";
import { useParams } from "react-router-dom";

import Overlay from "./Overlay";
import LogoIrcam from "./LogoIrcam";
import styles from "../styles/steps.module.css";
import { getScenesFromApi, getSceneIntro } from "../scenes";

const Title: React.FC<{ onHide?: () => void }> = ({ onHide }) => {
  getScenesFromApi();
  const { scene } = useParams<{ scene: string }>();

  // get intro scene
  const sceneIntro = getSceneIntro();
  const title = sceneIntro.scenario?.name;

  let isSceneIntro = false;
  if (sceneIntro.scenarioIdentifier == scene.slice(0, -3)) {
    isSceneIntro = true;
  }
  const startScene = useCallback(() => {
    if (typeof onHide === "function") onHide();
  }, [onHide]);
  const headerElement = () => (
    <div style={{ width: "100%", textAlign: "center" }}>
      <LogoIrcam />
    </div>
  );

  return (
    <>
      {isSceneIntro && (
        <Overlay full closeOnClick onClose={startScene} header={headerElement}>
          <h1 className={styles.intro}>{title}</h1>
          <img src={`${process.env.PUBLIC_URL}/finger.png`} width="15%" />
        </Overlay>
      )}
    </>
  );
};

export default Title;
