import React from "react";

const LogoIrcam: React.FC<{ width?: number; height?: number }> = (props) => {
  return <img src={`${process.env.PUBLIC_URL}/logoircam_blanc.png`} alt="Logo Ircam" {...props} />;
};

export default LogoIrcam;
