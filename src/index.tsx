import React from "react";
import ReactDOM from "react-dom";
import * as Sentry from "@sentry/react";
import { BrowserTracing } from "@sentry/tracing";
import { BrowserRouter as Router } from "react-router-dom";
import { RecoilRoot } from "recoil";
import "./styles/index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { XebraProvider } from "./hooks/xebra";

let isDev = false;

if (!process.env.NODE_ENV || process.env.NODE_ENV === "development") {
  // dev
  isDev = true;
} else {
  // production
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  console.log = function () {};
}

if (!isDev) {
  Sentry.init({
    dsn: "https://c3ed9517ff17455596319522b390a121@o1098110.ingest.sentry.io/4504114414026752",
    integrations: [new BrowserTracing()],

    // Set tracesSampleRate to 1.0 to capture 100%
    // of transactions for performance monitoring.
    // We recommend adjusting this value in production
    tracesSampleRate: 1.0,
  });
}

if (isDev) {
  ReactDOM.render(
    <React.StrictMode>
      <XebraProvider>
        <Router basename="/tact-ui">
          <RecoilRoot>
            <React.Suspense fallback={<div>Loading...</div>}>
              <App />
            </React.Suspense>
          </RecoilRoot>
        </Router>
      </XebraProvider>
    </React.StrictMode>,
    document.getElementById("root")
  );
} else {
  ReactDOM.render(
    <React.StrictMode>
      <Sentry.ErrorBoundary>
        <XebraProvider>
          <Router basename="/tact-ui">
            <RecoilRoot>
              <React.Suspense fallback={<div>Loading...</div>}>
                <App />
              </React.Suspense>
            </RecoilRoot>
          </Router>
        </XebraProvider>
      </Sentry.ErrorBoundary>
    </React.StrictMode>,
    document.getElementById("root")
  );
}

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
