import { useCallback, useEffect } from "react";
import { getHomes, getScenarios, getScenes, getZones, getSounds, getColors, getInfos, getCredits } from "../api/index";

export const useApi = (): (() => void) => {
  // API
  // declare the async data fetching function
  const fetchData = useCallback(async () => {
    const homes = await getHomes();
    console.log("HOMES: ", homes);

    const scenarios = await getScenarios();
    console.log("SCENARIOS: ", scenarios);

    const scenes = await getScenes();
    console.log("SCENES: ", scenes);

    const zones = await getZones();
    console.log("ZONES: ", zones);

    const sounds = await getSounds();
    console.log("SOUNDS: ", sounds);

    const colors = await getColors();
    console.log("COLORS: ", colors);

    const infos = await getInfos();
    console.log("INFOS: ", infos);

    const credits = await getCredits();
    console.log("CREDITS: ", credits);
  }, []);

  // the useEffect is only there to call `fetchData` at the right time
  useEffect(() => {
    fetchData()
      // make sure to catch any error
      .catch(console.error);
  }, [fetchData]);

  return fetchData;
};
