import { useCallback, useEffect } from "react";
import { useXebra, logData } from "../hooks/xebra";

export let isDebugMode = false;

export const useLogger = (): (() => void) => {
  const { sendMessage } = useXebra();

  const logger = useCallback(() => {
    useEffect(() => {
      // Logs
      function saveToFile() {
        const jsonObjectAsString = JSON.stringify(logData, null, 2);
        const blob = new Blob([jsonObjectAsString], {
          type: "octet/stream",
        });
        const anchor = document.createElement("a");
        anchor.id = "debug-log";
        anchor.download =
          "logs-xebra-" + new Date().toISOString().split("T")[0] + "-" + new Date().toLocaleTimeString() + ".json";
        anchor.href = window.URL.createObjectURL(blob);
        anchor.click();
        document.body.append(anchor);
      }
      window.addEventListener(
        "keydown",
        (event) => {
          if (event.defaultPrevented) {
            return;
          }
          switch (event.key) {
            case "L":
              isDebugMode = !isDebugMode;
              if (isDebugMode) {
                sendMessage("logchrome", "startlog");
              } else {
                sendMessage("logchrome", "stoplog");
                saveToFile();
              }
              break;
            case "l":
              isDebugMode = !isDebugMode;
              if (isDebugMode) {
                sendMessage("logchrome", "startlog");
              } else {
                sendMessage("logchrome", "stoplog");
                saveToFile();
              }
              break;
            default:
              return;
          }
          event.preventDefault();
        },
        true
      );
    }, []);
  }, [sendMessage]);

  return logger;
};
