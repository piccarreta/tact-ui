import { useEffect, useMemo, useState } from "react";
import * as effects from "../effects";
import { EffectRunner } from "../effects/typings";
import { GestureHandlers } from "../scenes/typings";
import { Scene } from "../api/typings";
import { CanvasGrid } from "../utils/grid";
import { noop } from "../utils/time";

export type Effects = Record<keyof typeof effects, EffectRunner>;
type VFXHookResult = [Effects, GestureHandlers | undefined];

export const useVFX = (grid: CanvasGrid, scene: string, sceneData: Scene): VFXHookResult => {
  const [gestures, setGestures] = useState<GestureHandlers>();
  const vfx = useMemo<Effects>(
    () => Object.fromEntries(Object.entries(effects).map(([name, fx]) => [name, grid ? fx(grid) : noop])) as Effects,
    [grid]
  );

  useEffect(() => {
    if (grid && scene) {
      if (sceneData.wet_fx) {
        import(`../scenes/gestures-wet`).then((mod) => setGestures(mod.default(grid, sceneData)));
      } else {
        import(`../scenes/gestures`).then((mod) => setGestures(mod.default(grid, sceneData)));
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [grid, scene]);

  return [vfx, gestures];
};
