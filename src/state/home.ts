import { selector } from "recoil";
import { getHomes } from "../api";

export const getHomesFromApi = selector({
  key: "getHomesFromApi",
  get: async () => {
    const response = await getHomes();
    return response;
  },
});
