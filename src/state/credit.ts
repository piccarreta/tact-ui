import { selector } from "recoil";
import { getCredits } from "../api";

export const getCreditsFromApi = selector({
  key: "getCreditsFromApi",
  get: async () => {
    const response = await getCredits();
    return response;
  },
});
