import { atom, selector, selectorFamily } from "recoil";
import { Scene } from "../api/typings";
import { getScenes } from "../api";

export const sceneState = atom<Scene>({
  key: "sceneState",
  default: { name: "" } as Scene,
});

export const scenesListState = atom<Record<string, Scene>>({
  key: "scenesListState",
  default: { list: {} } as unknown as Record<string, Scene>,
});

export const hasNext = selectorFamily<boolean, string>({
  key: "scene.hasNext",
  get:
    (creation) =>
    ({ get }) => {
      const current = get(sceneState);
      if (current.name === "credits") return false;
      if ((current.name ?? "").indexOf(creation) === 0) {
        return !!current.next;
      }
      return false;
    },
});

export const scenesApiState = selector({
  key: "scenesApiState",
  get: async () => {
    const response = await getScenes();
    return response;
  },
});
