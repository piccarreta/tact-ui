import { selector } from "recoil";
import { getInfos } from "../api";

export const getInfosFromApi = selector({
  key: "getInfosFromApi",
  get: async () => {
    const response = await getInfos();
    return response;
  },
});
