import { atom, selector } from "recoil";
import { Scenario } from "../api/typings";
import { getScenarios } from "../api";

export const scenarioState = atom<Scenario>({
  key: "scenarioState",
  default: { name: "" } as Scenario,
});

export const getScenariosFromApi = selector({
  key: "getScenariosFromApi",
  get: async () => {
    const response = await getScenarios();
    return response;
  },
});
