import { useEffect } from "react";
import { atom, selector } from "recoil";
import { useXebra } from "../hooks/xebra";
import { useHardRefresh } from "../hooks/location";

import { w3cwebsocket as W3CWebSocket } from "websocket";

export const webSocketState = atom({
  key: "webSocketState",
  default: process.env.REACT_APP_WEBSOCKET_IP || "127.0.0.1",
});

export const connectToLocal = selector({
  key: "webSocket.Local",
  set: ({ set }) => set(webSocketState, "192.168.0.2"),
  get: () => "192.168.0.2",
});
export const connectToFred = selector({
  key: "webSocket.Fred",
  set: ({ set }) => set(webSocketState, "82.64.145.9"),
  get: () => "82.64.145.9",
});

export const connectToVigie = selector({
  key: "webSocket.Vigie",
  set: ({ set }) => set(webSocketState, "129.102.67.242"),
  get: () => "129.102.67.242",
});

export function updateMaxPatch(): unknown {
  const { sendMessage } = useXebra();
  const refresh = useHardRefresh(process.env.PUBLIC_URL + "/tact-ui");

  // WebSocket connection with backend
  return useEffect(() => {
    // const client = new W3CWebSocket("ws://127.0.0.1:8000/ws/" + roomName + "/");
    const client = new W3CWebSocket("ws://127.0.0.1:8080/ws/tact-backend/");

    client.onopen = () => {
      console.info("[TACT-BACKEND] WebSocket Client Connected");
    };
    client.onmessage = (message) => {
      // console.log(message);
      const dataFromServer = JSON.parse(message.data as string);
      if (dataFromServer) {
        console.info("msg: ", dataFromServer.message);
        console.info("type: ", dataFromServer.type);

        if (dataFromServer.type === "update_model") {
          refresh();
        }

        if (dataFromServer.type === "update_scenario") {
          if (window.location.href.endsWith("/info")) {
            const url = window.location.href.split("/info")[0];
            window.location.href = url + "/play";
          } else {
            window.location.reload();
          }
        }

        if (dataFromServer.type === "update_app") {
          // send message to Max patch via Xebra
          sendMessage("backend", "update");
          console.info("[TACT-BACKEND] Update Max patch");

          refresh();
        }
      }
    };
    client.onerror = () => {
      console.error("[TACT-BACKEND] WebSocket Client Error");
    };
  }, [refresh, sendMessage]);
}
