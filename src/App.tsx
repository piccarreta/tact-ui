import React from "react";
import { Switch, Route } from "react-router-dom";
import { useEvent, useIdle } from "react-use";
import { FormattedMessage } from "react-intl";

import * as Step from "./steps";

import Canvas from "./components/Canvas";
import Title from "./components/Title";
import IntlProvider from "./components/IntlProvider";
import ShowAfterIf from "./components/ShowAfterIf";
import Overlay from "./components/Overlay";
// import ProgressBar from "./components/ProgressBar";
import { getScenesFromApi, getSceneIntro } from "./scenes";

import { updateMaxPatch } from "./state";
import { useXebra } from "./hooks/xebra";
import { useLogger } from "./hooks/logger";

import styles from "./styles/steps.module.css";

const preventDefault: EventListener = (e) => e.preventDefault();

const App: React.FC = () => {
  const isIdle = useIdle(18e4, false); // 3 minutes

  // get intro scene
  getScenesFromApi();
  const sceneIntro = getSceneIntro();

  // WebSocket connection with backend
  updateMaxPatch();

  // Mira WebSocket connection
  const { checkStatus } = useXebra();
  const isConnected = checkStatus(4);
  console.log("[TACT-UI] IS CONNECTED TO MIRA: ", isConnected);

  // logger
  const logger = useLogger();
  logger();

  // Prevent default pinching
  useEvent("gesturestart", preventDefault, document);
  useEvent("gesturechange", preventDefault, document);
  // Prevent right-clicking
  useEvent("contextmenu", preventDefault, document);
  // Prevent scrolling and wheeling
  useEvent("scroll", preventDefault, document);
  useEvent("wheel", preventDefault, document);

  return (
    <IntlProvider>
      <div id="play-overlay-warning" className={styles.warning_container}>
        <Overlay className={styles.warning} header={null} footer={null}>
          <FormattedMessage id="play.warning.touches" tagName="p" />
        </Overlay>
      </div>
      <Switch>
        <Route path="/" exact>
          <Step.Home />
        </Route>
        <Route path="/info" exact>
          <Step.Home />
          <Step.Info />
        </Route>
        <Route path="/:scene">
          <Title />
          <Canvas />
          <Step.Next />
        </Route>
      </Switch>
      <Route>
        <ShowAfterIf condition={isIdle && sceneIntro != undefined} children={<Step.ScreenSaver />} />
      </Route>
      <Switch>
        <Route path="/:scene" exact>
          {({ location }) =>
            sceneIntro ? location.pathname.startsWith(sceneIntro.scenarioIdentifier) ? <Step.Start /> : null : null
          }
        </Route>
        <Route path="/:scene/play">
          <Step.Play />
        </Route>
        <Route path="/:scene/refresh">
          <Step.Refresh />
        </Route>
        <Route path="/:scene/info">
          <Step.Info />
        </Route>
        <Route path="/:scene/confirm">
          <Step.Refresh />
        </Route>
      </Switch>
    </IntlProvider>
  );
};

export default App;
