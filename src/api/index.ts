import { requestJSON } from "./request";
import { HomeApi, ScenarioApi, SceneApi, InfoApi, CreditApi } from "../api/typings";

export async function getHomes(): Promise<HomeApi[]> {
  return await requestJSON(process.env.REACT_APP_API_ROOT + "/api/homes/");
}

export async function getScenarios(): Promise<ScenarioApi[]> {
  return await requestJSON(process.env.REACT_APP_API_ROOT + "/api/scenarios/");
}

export async function getScenes(): Promise<SceneApi[]> {
  return await requestJSON(process.env.REACT_APP_API_ROOT + "/api/scenes/");
}

export async function getZones(): Promise<unknown> {
  return await requestJSON(process.env.REACT_APP_API_ROOT + "/api/zones/");
}

export async function getSounds(): Promise<unknown> {
  return await requestJSON(process.env.REACT_APP_API_ROOT + "/api/sounds/");
}

export async function getColors(): Promise<unknown> {
  return await requestJSON(process.env.REACT_APP_API_ROOT + "/api/colors/");
}

export async function getInfos(): Promise<InfoApi[]> {
  return await requestJSON(process.env.REACT_APP_API_ROOT + "/api/infos/");
}

export async function getCredits(): Promise<CreditApi[]> {
  return await requestJSON(process.env.REACT_APP_API_ROOT + "/api/credits/");
}
