type authors = [
  {
    username: string;
  }
];

type coverImage = {
  file: string;
};

type backgroundImage = {
  file: string;
};

type foregroundImage = {
  file: string;
};

type graphFxOnMove = {
  id: number;
  enabled: boolean;
  brush_radius: number;
  accuracy: number;
  hardness: number;
};

type graphFxAfterMove = {
  id: number;
  enabled: boolean;
  brush_radius: number;
};

type graphFxAfterTap = {
  id: number;
  enabled: boolean;
  rain_enabled: boolean;
  brush_radius: number;
  after_tap_fx: number;
};

type graphFxOnHold = {
  id: number;
  enabled: boolean;
  max_radius: number;
  max_width: number;
  max_height: number;
  shape: number;
  display_speed: number;
};

export type InfoApi = {
  id: number;
  name: string;
  artistic_direction: string;
  production: string;
  coproduction: string;
  rim: string;
  design: string;
  furniture_design: string;
  furniture_realisation: string;
  programming: string;
  cultural_engineering: string;
  production_manager: string;
};

type RoleApi = {
  id: number;
  name: string;
};

export type Credit = {
  data: CreditApi[];
  hasCredits: boolean;
  credits: string;
};

export type CreditApi = {
  id: number;
  scenario: ScenarioApi;
  role: RoleApi;
  first_name: string;
  last_name: string;
};

export type HomeApi = {
  id: number;
  name: string;
  scenario: ScenarioApi[];
  status: number;
  enabled: boolean;
};

export type ScenarioApi = {
  id: number;
  identifier: string;
  name: string;
  authors: authors;
  status: number;
  is_intro: boolean;
  cover_image: coverImage;
  credit_duration: number;
};

export type SceneApi = {
  id: number;
  identifier: string;
  name: string;
  authors: authors;
  status: number;
  background_image: backgroundImage;
  foreground_image: foregroundImage;
  scenario: ScenarioApi;
  order: number;
  next_duration: string;
  transition: number;
  transition_color: number;
  transition_duration: number;
  wet_fx: boolean;
  graph_fx_on_move: graphFxOnMove;
  graph_fx_after_move: graphFxAfterMove;
  graph_fx_after_tap: graphFxAfterTap;
  graph_fx_on_hold: graphFxOnHold;
};

// type Transition = number | string | ((canvas: HTMLCanvasElement, image: HTMLImageElement) => Promise<void>);
type Transition = number;

export type Scenario = {
  name: string;
  title: string;
  composer?: string;
  designer?: string;
  photographer?: string;
  assistantComposer?: string;
  code?: string;
  imageSrc: string;
  duration: number;
  credit_duration: number;
  transitionIn?: Transition;
  transitionOut?: Transition;
  next?: string;
  acknowledgements?: string;
};

export type Scene = {
  name: string;
  title: string;
  scenario?: ScenarioApi;
  scenarioIdentifier: string;
  composer?: string;
  designer?: string;
  photographer?: string;
  assistantComposer?: string;
  code?: string;
  imageSrc: string;
  wetImageSrc: string;
  duration: number;
  transitionIn?: Transition;
  transitionOut?: Transition;
  transitionColor?: Color;
  order?: number;
  next?: string;
  totalScenesNum?: number;
  acknowledgements?: string;
  wet_fx: boolean;
  graphFxOnMove?: graphFxOnMove;
  graphFxAfterMove?: graphFxAfterMove;
  graphFxAfterTap?: graphFxAfterTap;
  graphFxOnHold?: graphFxOnHold;
};
