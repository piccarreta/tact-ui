variables:
  WEBSOCKET_IP:
    value: "127.0.0.1"
    description: "The Mira websocket IP address"

stages:
  - build
  - deploy

workflow:
  rules:
    # avoid duplicate pipeline when a merge request is open
    # see https://docs.gitlab.com/ee/ci/yaml/workflow.html#switch-between-branch-pipelines-and-merge-request-pipelines
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - if: '$CI_COMMIT_BRANCH'

build:
  image: node:14-alpine
  only:
    - master
    - dev
  stage: build
  before_script:
    - yarn
  script:
    - REACT_APP_WEBSOCKET_IP=$WEBSOCKET_IP yarn build
  cache:
    - paths:
      - node_modules
      key:
        files:
          - yarn.lock
  artifacts:
    paths:
      - build

# reusable script to setup SSH
.prepare_ssh:
  before_script:
    - which ssh-agent || (apk add --update openssh-client)
    - mkdir "$HOME/.ssh" && chmod 0700 "$HOME/.ssh"
    - echo "$SSH_PRIVATE_KEY" > "$HOME/.ssh/id_ed25519" && chmod 0700 "$HOME/.ssh/id_ed25519"
    - echo "IdentityFile $HOME/.ssh/id_ed25519" > "$HOME/.ssh/config"
    - |
      cat << EOF >> "$HOME/.ssh/config"
      Host prod-git
        User pow
        HostName gitforum01.ircam.fr
        StrictHostKeyChecking no
      EOF
    - eval "$(ssh-agent -s)"
    - ssh-add $HOME/.ssh/id_ed25519

# reusable deploy script
# store the script in the $DEPLOY_SCRIPT variable
.deploy:
  script:
    # define env var containing deploy tact script to be run on the server
    - |
      read -r -d '' DEPLOY_TACT_SCRIPT << EOT || true
      #!/bin/bash
      # -x: print the executed commands to stdout.
      # -e: execution stop when one of the commands fails.
      set -x -e
      # prepare directory
      whoami
      hostname
      ls -la /srv/play/tact-ui
      EOT

deploy_prod:
  stage: deploy
  image: alpine
  variables:
    GIT_STRATEGY: none
  rules:
    - if: '$CI_DEFAULT_BRANCH == $CI_COMMIT_REF_NAME'
  # avoid concurrent deployments
  resource_group: production
  dependencies:
    - build
  before_script:
    - which curl || (apk add --update curl)
    - apk add rsync
    # ssh key
    - !reference [.prepare_ssh, before_script]
  script:
    - !reference [.deploy, script]
    # update tact
    - rsync -a --delete build/ prod-git:/srv/play/tact-ui/
    - echo "$DEPLOY_TACT_SCRIPT" | ssh prod-git /bin/bash
    # wait for http response 200, timeout after 2 minutes
    # - timeout 120 sh -c 'while [[ "$(curl -s -o /dev/null -w ''%{http_code}'' https://play.forum.ircam.fr/tact-ui)" != "200" ]]; do sleep 5; done' || false
